# build stage
FROM node:10.15 as build-stage
MAINTAINER williewu

RUN npm config set registry https://registry.npm.taobao.org

WORKDIR /app
COPY . .

RUN npm install
RUN npm run build

# production stage
FROM nginx:1.17.9-alpine as production-stage


COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
